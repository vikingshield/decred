# BUILD
FROM golang:1.15.5 AS build
ENV GOBIN=/go/bin 
ENV GOPATH=/go
ENV CGO_ENABLED=0
ENV GOOS=linux

RUN apt update -q && apt install -y git

WORKDIR /app
RUN git clone https://github.com/vikingshield/dcrwallet.git \
    && cd dcrwallet \
    && git checkout release-v1.6 \
    && go build

COPY . .



FROM alpine:3.12.1

ENV DCRD_VERSION=1.6.3
ENV DCRD_DATA=/home/decred/.decred
ENV GLIBC_VERSION=2.28-r0
WORKDIR /opt/decred

RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
	&& wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk \
	&& wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk

RUN apk update \
	&& apk --no-cache add ca-certificates gnupg bash su-exec \
	&& apk --no-cache add glibc-${GLIBC_VERSION}.apk \
	&& apk --no-cache add glibc-bin-${GLIBC_VERSION}.apk


RUN apk update \
	&& apk --no-cache add ca-certificates gnupg bash su-exec expect

COPY ./key.txt /opt/decred/key.txt
RUN gpg --import  < /opt/decred/key.txt

# Do not use dcrwallet from official repo
RUN wget https://github.com/decred/decred-binaries/releases/download/v${DCRD_VERSION}/decred-v${DCRD_VERSION}-manifest.txt \
    && wget https://github.com/decred/decred-binaries/releases/download/v${DCRD_VERSION}/decred-v${DCRD_VERSION}-manifest.txt.asc \
    && gpg --verify decred-v${DCRD_VERSION}-manifest.txt.asc \
    && wget https://github.com/decred/decred-binaries/releases/download/v1.6.3/decred-linux-amd64-v${DCRD_VERSION}.tar.gz \
    && grep decred-linux-amd64-v${DCRD_VERSION}.tar.gz decred-v${DCRD_VERSION}-manifest.txt | sha256sum -c \
    && mkdir dcrd \
    && tar xzvf decred-linux-amd64-v${DCRD_VERSION}.tar.gz \
    && mkdir /root/.dcrd \
    && cd decred-linux-amd64-v${DCRD_VERSION} && mv dcrd dcrctl /usr/local/bin \
    && apk del wget ca-certificates \
    && cd .. && rm -rf dcr* glibc-*  

COPY --from=build /app/dcrwallet/dcrwallet /usr/local/bin/
RUN dcrwallet --version

RUN adduser -S decred
COPY ./scripts /scripts

COPY ./dcrdcerts /dcrdcerts
COPY ./dcrwalletcerts /dcrwalletcerts


EXPOSE 19557

ENTRYPOINT ["/scripts/entrypoint.sh"]
CMD ["dcrd"]
